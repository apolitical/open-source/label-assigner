label-assigner
==============

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

Outline
-------

Open-source version of our internal `bucketeer` package.

This Python service assigns buckets to articles in [Contentful](https://app.contentful.com/). 

The pipeline has two main stages:

1. Training a machine-learning model.
    - Reading in content from the Apolitical platform
    - Converting each article into a TF-IDF representation (machine readable features)
    - Reducing the feature space using the NMF approach
    - Importing labelled training examples from a Postgres database for model creation
    - Generating one categoriser model per user-defined bucket (trained on the labelled examples). The set of trained models use a linear Support Vector Machine architecture.       
2. Listening for messages in Pub/Sub.    

Pub/Sub topics have been created for each space in Contentful. When a new article is published in a space, a webhook fires, triggering a cloud function to publish details of the article in the corresponding topic.

This service has a subscription to a topic and pulls new articles, skipping revisions, when a new article is published in Contentful. It retrieves the body of the article, runs the text through the trained model and appends new buckets to the article.

Development instructions
------------------------

The code is hosted on GitLab. Once you've cloned the repository you will need to fire up the virtual environment:

1. Make sure that you have the `pipenv` library: `pip install pipenv --upgrade`.
2. In the top level directory, run `pipenv install --dev` (installs virtual environment with development tools).
3. Fire up the test suite in the virtual environment using `pipenv run ptw`. Edit at your leisure.
4. There are linting and static type checks that can be run in the virtual environment using `pipenv run lint` and `pipenv run mypy`. Use `pipenv run lint-fix` to fix formatting errors.

Instructions for running locally
--------------------------------

In order to instead *run* the procedure via your local machine (without laboriously installing the requirements and eventually running `bucketeer/pipeline.py`):

1. Follow steps 1 and 2 in 'Development instructions', above.
2. To run the production code locally, you must first configure the repository. Use the `/bucketeer/config/*.example` and `/bucketeer/*.example` files to find out how to make the required configuration files.
3. Use `pipenv run pipeline` (script defined in the Pipfile) in order to run the script.

Notes for open source version
-----------------------------

For the open-source version of this code, we are providing production code and the test suite/QA checks.

However, we have removed elements of the project specific to our deployment and production infrastructure, as well as model-validation code. This open-source version is not intended to be a drag-and-drop solution.

If you are working on a similar project you will need

- a Contentful space for publishing articles
- a Google Cloud Platform account to set up Pub/Sub, Cloud Storage, and Cloud Functions
- a postgres database with your training labels (this could also be set up on GCP)
- deployment infrastructure (e.g. build a docker image, store in a container registry, and deploy to a GKE cluster)

Contributors
------------

- Paddy Alton (paddy.alton@apolitical.co)
- Jordan Lueck (jordan.lueck@apolitical.co)
- Maria Soloveva (maria.soloveva@apolitical.co)
- CY Yang (cy.yang@apolitical.co)

(with thanks to the engineering team for assistance and review)
