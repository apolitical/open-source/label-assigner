# bucketeer.postgres
# manages connection with PostgreSQL DB


from os import getenv
from time import time, sleep
from base64 import b64decode
from pandas import DataFrame, Series
from tempfile import NamedTemporaryFile
from sqlalchemy import create_engine
from sqlalchemy.exc import ResourceClosedError
from sqlalchemy.engine import Connection, ResultProxy
from contextlib import closing

# type checking imports
from pandas import Series
from typing import Union, Dict, Optional, Mapping


class DBLink:

    """
    DBLink

    Class for communicating with an external SQL database

    """

    def __init__(
        self,
        connection_string: str,
        withssl: bool = False,
        ssl: Optional[Mapping[str, Union[Dict[str, str], str]]] = None,
    ):

        """
        __init__ for DBLink

        INPUTS:
            connection_string - of format:

                <dialect>://<user>:<password>@<ip_address>/<database_name>

        KEYWORDS:
            withssl = False - whether to connection with SSL arguments
                ssl = None  - dictionary of SSL arguments

        """

        connect_args = {"connect_args": ssl} if withssl else {}

        self.engine = create_engine(connection_string, **connect_args)

    def get_connection(self, timeout: float = 10.0) -> Connection:

        """
        get_connection

        This method returns a connection object, the result of invoking
        self.engine.connect()

        KEYWORDS:
            timeout = 10. - number of seconds for which to keep trying

        OUTPUTS:
            connection - sqlalchemy connection

        """

        start = time()
        connection = None

        last_error = Exception("Undefined Exception")

        while time() < start + timeout:
            try:
                return self.engine.connect()
            except Exception as this_error:
                last_error = this_error
                sleep(1)  # wait 1s then retry

        raise last_error  # if we time out without returning

    def execute(self, sql: str) -> ResultProxy:

        """
        execute

        This method executes a provided SQL query.

        INPUTS:
            sql - a query to be executed

        OUTPUTS:
            response - SQLalchemy response; expect to have .fetchall
                       and .keys methods

        """

        with closing(self.get_connection()) as connection:
            return connection.execute(sql)

    def query_to_dataframe(self, sql: str) -> DataFrame:

        """
        query_to_dataframe

        This method executes a provided SQL query and passes the result
        into a pandas DataFrame

        INPUTS:
            sql - a query to be executed

        OUTPUTS:
            table - pandas DataFrame containing the result

        """

        response = self.execute(sql)

        try:
            table = DataFrame(response.fetchall(), columns=response.keys())
        except ResourceClosedError:
            table = DataFrame()

        return table


def env2file(env: str) -> str:

    """
    env2file

    Writes a base64-encoded environment variable to a temporary file.

    Returns the path to that file.

    """

    val = getenv(env)

    if val is None:
        raise EnvironmentError(f"Missing variable {env} from environment!")

    decoded = [  # bytes, decoded from val, split on newline, line-delimiters reattached
        line + b"\n" for line in b64decode(val).split(b"\n")
    ]

    with closing(NamedTemporaryFile(delete=False)) as tfil:
        tfil.writelines(decoded)
        return tfil.name


def get_db_client_pg() -> DBLink:

    """
    get_db_client_pg

    This function returns an authenticated instance of a client suitable
    for connecting to an external PostgreSQL database

    OUTPUTS:
        db_link - authenticated instance of Link

    """

    pg_connection_string = getenv("PG_CONNECTION_STRING")

    if pg_connection_string is None:
        raise EnvironmentError("Missing variable PG_CONNECTION_STRING from environment")

    if getenv("RUNNING_LOCALLY") == "True":
        ssl_args = {
            "sslcert": env2file("PG_SSL_CERT"),
            "sslkey": env2file("PG_SSL_KEY"),
        }
        db_link = DBLink(pg_connection_string, withssl=True, ssl=ssl_args)
    else:
        db_link = DBLink(pg_connection_string)

    return db_link


def get_training_data(db_client: DBLink) -> Series:

    """
    get_training_data

    Function that returns the content of labelled_article table in
    bucketeer-training-data database of PostgreSQL instance

    INPUTS:
        db_client - instantiated Client for connecting to an external
                    database

    OUTPUTS:
        training_data - pandas Series containing a list of bucket IDs,
                    indexed by article slug

    """

    training_data_df = db_client.query_to_dataframe(
        "SELECT article_slug, bucket_id FROM labelled_article"
    )
    training_data_df = training_data_df.groupby(["article_slug"]).agg(lambda x: list(x))
    training_data = training_data_df.squeeze()

    return training_data


def get_bucket_names(db_client: DBLink) -> Series:

    """
    get_bucket_names

    Function that returns the content of bucket_name table in
    bucketeer-training-data database of PostgreSQL instance

    INPUTS:
        db_client - instantiated Client for connecting to an external
                    database

    OUTPUTS:
        bucket_names - pandas Series containing bucket names,
                    indexed by bucket id

    """

    bucket_name_df = db_client.query_to_dataframe(
        "SELECT bucket_id, name FROM bucket_name"
    ).set_index("bucket_id")

    bucket_names = bucket_name_df.squeeze()

    return bucket_names
