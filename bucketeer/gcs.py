# bucketeer.gcs
# manages GCS communication and processing of data

import json
import pandas as pd

# type checking imports
from pandas import Series
from typing import Dict, Any, List, Tuple

from google.cloud.storage import Client as GCSClient


def get_content() -> Series:

    """
    get_content

    Top-level wrapper for all functions processing data from Contentful Dump

    OUTPUTS:
        articles_full - pandas Series containing the content of Solution Articles,
                        indexed by article slug

    """

    my_gcs_client = GCSClient()
    all_entries = get_latest_contentful_entries(my_gcs_client)

    articles, articles_with_case_studies, case_studies = get_relevant_articles(
        all_entries
    )
    extra_content = merge_dicts(articles_with_case_studies, case_studies)
    articles_full = extend_content(articles, extra_content)

    return articles_full


def get_latest_contentful_entries(
    my_gcs_client: GCSClient, bucket_name: str = "contentful_content"
) -> Dict[str, List[Dict[str, Any]]]:

    """
    get_latest_contentful_entries

    Download the latest Contentful data from GCS bucket

    INPUTS:
        gcs_client - GCSClient, connection to Google Cloud Storage

    KEYWORDS:
        bucket_id - str = "contentful_content", name of the GCS bucket
                    with Contentful data

    OUTPUTS:
        contentful_data - dict, Contentful data

    """

    bucket = my_gcs_client.bucket(bucket_name=bucket_name)
    blobs = bucket.list_blobs()

    # get latest Contentful dump file name
    latest_dump_file = sorted(
        [b.name for b in blobs if b.name.endswith("data.json")], reverse=True
    )[0]

    contentful_data = json.loads(bucket.blob(latest_dump_file).download_as_bytes())

    return contentful_data


def get_relevant_articles(
    contentful_data: Dict[str, List[Dict[str, Any]]]
) -> Tuple[Dict[str, str], Dict[str, str], Dict[str, str]]:

    """
    get_relevant_articles

    Retrieve relevant information on Solution Articles and Case Studies
    from GCS Contentful data dump

    INPUTS:
        contentful_data - dict, data dump of Contentful contents

    OUTPUTS:
        slug_to_body_map - dict mapping Solution Articles to their content
        slug_to_case_study_map - dict mapping Solution Articles of case-study type
                                 to relevant Case Study entries
        case_study_to_extra_body_map - dict mapping Case Studies to their content

    """

    slug_to_body_map = dict()
    slug_to_case_study_map = dict()
    case_study_to_extra_body_map = dict()

    for entry in contentful_data["entries"]:
        entry_type = entry["sys"]["contentType"]["sys"]["id"]

        if entry_type == "solutionArticle":
            solution_article_type = entry["fields"].get("type", {}).get("en")
            article_slug = entry["fields"]["slug"]["en"]
            article_body = entry["fields"].get("body", {}).get("en", "")

            slug_to_body_map[article_slug] = article_body

            if solution_article_type == "case-study":
                case_study_id = entry["fields"]["caseStudy"]["en"]["sys"]["id"]

                slug_to_case_study_map[article_slug] = case_study_id

        if entry_type == "solutionArticleCaseStudy":
            case_study_id = entry["sys"]["id"]
            case_study_body = entry["fields"].get("fullStory", {}).get("en")

            if case_study_body:

                case_study_to_extra_body_map[case_study_id] = case_study_body

    return slug_to_body_map, slug_to_case_study_map, case_study_to_extra_body_map


def merge_dicts(slug_id_map: Dict[str, str], id_body_map: Dict[str, str]) -> Series:

    """
    merge_dicts

    Create a mapping from Solution Articles to the content of Case Studies

    INPUTS:
        slug_id_map - dict mapping Solution Articles with Case Studies
                        to relevant entries
        id_body_map - dict mapping Case Studies to their content

    OUTPUTS:
        slug_body_map_pd - Series containing full stories of Case Studies,
                        indexed by Solution Article slugs
    """

    slug_body_map = {
        slug: id_body_map.get(id, None) for slug, id in slug_id_map.items()
    }
    slug_body_map_pd = pd.Series(slug_body_map).dropna(axis=0)

    return slug_body_map_pd


def extend_content(articles: Dict[str, str], extra_content: Series) -> Series:

    """
    extend_content

    INPUTS:
        articles - dict mapping Solution Articles to their content
        extra_content - Series containing full stories of Case Studies,
                        indexed by Solution Article slugs

    OUTPUTS:
        full_content - pandas Series containing the content of Solution articles,
                        indexed by article_slug

    """

    articles_pd = pd.DataFrame.from_dict(articles, orient="index", columns=["content"])
    extra_content.name = (
        "extra_content"  # Series must have a name to join with a DF below
    )
    extended_articles = articles_pd.join(extra_content).fillna("")

    extended_articles.loc[:, "full_content"] = extended_articles.apply(
        lambda row: row.content + " \n " + row.extra_content, axis=1
    )

    full_content = extended_articles["full_content"]
    full_content.index.name = "article_slug"

    return full_content
