# bucketeer.modelling_params.py

from bucketeer.custom_types import (
    ParamGridType,
    SGDkwargsType,
    LRkwargsType,
    SVCkwargsType,
)


def get_grid_params() -> ParamGridType:

    """
    get_grid_params

    Returns a dictionary of keyword arguments for Grid Search with SGDClassifier
    (this allows for easy configuration of the model)

    """

    param_grid: ParamGridType = {
        "sgd__loss": ["hinge", "log", "modified_huber", "perceptron"],
        "sgd__alpha": [0.0001, 0.001, 0.01],
        "sgd__tol": [1e-4, 1e-3, 1e-2],
        "sgd__epsilon": [0.1, 0.01, 0.001],
    }

    return param_grid


def get_sgd_kwargs(baseline=False):

    """
    get_sgd_kwargs

    Returns a dictionary of keyword arguments for the Scikit-Learn SGDClassifier
    vectoriser (this allows for easy configuration of the model)

    """

    sgd_kwargs: SGDkwargsType = {
        "penalty": "l2",
        "l1_ratio": 0.15,
        "fit_intercept": True,
        "max_iter": 1000,
        "shuffle": True,
        "verbose": 0,
        "n_jobs": None,
        "random_state": 42,
        "learning_rate": "optimal",
        "eta0": 0.0,
        "power_t": 0.5,
        "early_stopping": False,
        "validation_fraction": 0.1,
        "n_iter_no_change": 5,
        "class_weight": None,
        "warm_start": False,
        "average": False,
    }

    if baseline:
        sgd_kwargs: SGDkwargsType = {
            **sgd_kwargs,
            **{"loss": "hinge", "alpha": 0.0001, "tol": 0.001, "epsilon": 0.1},
        }

    return sgd_kwargs


def get_lr_kwargs():

    """
    get_lr_kwargs

    Returns a dictionary of keyword arguments for the Scikit-Learn LogisticRegression classifier
    (this allows for easy configuration of the model)

    """

    lr_kwargs: LRkwargsType = {
        "penalty": "l2",
        "dual": False,
        "tol": 1e-4,
        "C": 1.0,
        "fit_intercept": True,
        "intercept_scaling": 1,
        "class_weight": None,
        "random_state": None,
        "solver": "liblinear",
        "max_iter": 100,
        "multi_class": "auto",
        "verbose": 0,
        "warm_start": False,
        "n_jobs": None,
        "l1_ratio": None,
    }

    return lr_kwargs


def get_svc_kwargs():

    """
    get_svc_kwargs

    Returns a dictionary of keyword arguments for the Scikit-Learn SVC classifier
    (this allows for easy configuration of the model)

    """
    svc_kwargs: SVCkwargsType = {
        "C": 1.0,
        "kernel": "rbf",
        "degree": 3,
        "gamma": "auto",
        "coef0": 0.0,
        "shrinking": True,
        "probability": True,
        "tol": 1e-4,
        "verbose": False,
        "max_iter": -1,
        "decision_function_shape": "ovr",
        "random_state": None,
    }

    return svc_kwargs
