# bucketeer.features_extracting

# basic imports
import numpy as np

# sklearn imports
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import NMF
from sklearn.preprocessing import Normalizer

# type-hinting imports
from pandas import Series
from typing import Dict
from bucketeer.custom_types import TfidfKwargsType


def get_tfidf_kwargs(min_df: int, max_df: float) -> TfidfKwargsType:

    """
    get_tfidf_kwargs

    Returns a dictionary of keyword arguments for the Scikit-Learn TF-IDF
    vectoriser (this allows for easy configuration of the model)

    """

    tfidf_kwargs: TfidfKwargsType = {
        "input": "content",  # read from data, not file/file object
        "encoding": "utf-8",  # expect UTF8 encoding
        "decode_error": "strict",  # raise exception if encoding is bad
        "strip_accents": "unicode",  # map accented characters to standard
        "lowercase": True,  # map to lowercase
        "preprocessor": None,  # already pre-processed
        "tokenizer": None,
        "analyzer": "word",  # word or character
        "stop_words": "english",  # removal of most common English words moved to pre-processing (before lemmatisation)
        "token_pattern": "(?u)\\b\\w\\w+\\b",  # how to identify a word
        "ngram_range": (1, 3),  # are terms single words (1,1), or n-grams (1,n)?
        "max_df": max_df,  # more filtering of common words (specific to our corpus)
        "min_df": min_df,  # ignore words occurring in fewer than min_df documents
        "max_features": None,  # prune features?
        "vocabulary": None,  # pre-choose terms?
        "binary": False,  # TF -> "True/False is term in document?
        "norm": "l2",  # normalize the TF-vectors for each document? L1 or L2?
        "use_idf": True,  # if false, just TF, not TF-IDF
        "smooth_idf": True,  # add 1 to DF (prevents zero divisions)
        "sublinear_tf": True,  # False, # TF -> 1 + log(TF); prevents v. high TFs dominating
    }

    return tfidf_kwargs


def feature_engineering(
    tfidf_min_df: int, tfidf_max_df: float, nmf_latent_topics: int
) -> Pipeline:
    """
    feature_engineering

    Function to initialize Pipeline object for features extraction.

    INPUT:
        tfidf_min_df - TF-IDF Vectorizer parameter, minimum document frequency
        tfidf_max_df - TF-IDF Vectorizer parameter, maximum document frequency
        nmf_latent_topics - number of latent features for NMF

    OUTPUT:
        pipe - Pipeline object to transform input data into machine readable features.

    """

    tfidf_kwargs = get_tfidf_kwargs(tfidf_min_df, tfidf_max_df)

    pipe = Pipeline(
        [
            ("tfidf", TfidfVectorizer(**tfidf_kwargs)),
            ("nmf", NMF(n_components=nmf_latent_topics)),
            ("norm", Normalizer(norm="l2")),
            ("skip_estimator", "passthrough"),
        ]
    )

    return pipe
