# bucketeer.app
# start-up file: trains a model and starts a listener

# general imports
from dotenv import load_dotenv
from functools import partial
from os import getenv
from pandas import DataFrame

from google.cloud.pubsub_v1 import SubscriberClient

# pipeline procedure imports
from bucketeer.postgres import (
    get_db_client_pg,
    get_training_data,
    get_bucket_names,
)
from bucketeer.pre_processing import (
    get_clean_text,
    filter_training_data,
    flatten_labels,
)
from bucketeer.feature_extraction import feature_engineering
from bucketeer.modelling import (
    create_model_container,
    training_procedure,
)
from bucketeer.contentful_api import Article
from bucketeer.gcs import get_content

from bucketeer.callback_utilities import (
    check_if_short,
    assign_buckets,
    get_buckets_to_write,
    write_buckets,
)

# type checking imports
from typing import Dict, Tuple, Sequence
from pandas import DataFrame
from google.cloud.pubsub_v1.subscriber.message import Message
from sklearn.pipeline import Pipeline
from sklearn.linear_model import SGDClassifier


def train_model() -> Tuple[Dict[str, SGDClassifier], Pipeline, Sequence[str]]:

    """
    train_model

    Trains a classifier.

    Pipeline Training Work Flow:
        1. Get Data and Labels
        2. Pre-processing
        3. Feature Extraction
        4. Model Training

    OUTPUTS:
        trained_model - trained classification model.
        features_pipeline - object with a fitted vectorizer.
        bucket_names - bucket labels to assign.

    """

    classification_model = getenv("CLASSIFICATION_MODEL", "SGD+ADASYN")
    # ["BASELINE", "GRIDSEARCH", "SGD+SMOTE", "SGD+ADASYN", "LR", "SVM"]

    ### 1. Getting Data
    print("Downloading Training Labels from PostgreSQL instance")
    pg_client = get_db_client_pg()
    raw_labels = get_training_data(pg_client)
    bucket_names = get_bucket_names(pg_client)

    print("Retrieving Body Field for Training Data from GCS Contentful Dump")
    articles = get_content()
    training_articles = articles.loc[articles.index.isin(raw_labels.index)]

    ### 2. Pre-Processing
    print("Pre-Processing Training Data")
    filtered_content = filter_training_data(
        training_articles, length_threshold=int(getenv("LENGTH_THRESHOLD", "250"))
    )
    cleaned_content = get_clean_text(filtered_content)

    raw_labels = raw_labels.loc[cleaned_content.index]
    y_train = flatten_labels(raw_labels, bucket_names)

    ### 3. Features Extraction
    print("Transforming Train Set into Features")
    features_pipeline = feature_engineering(
        tfidf_min_df=int(getenv("TFIDF_MIN_DF", "5")),
        tfidf_max_df=float(getenv("TFIDF_MAX_DF", "0.4")),
        nmf_latent_topics=int(getenv("NMF_LATENT_TOPICS", "50")),
    )  # initialise the object to pass further
    X_train = DataFrame(
        features_pipeline.fit_transform(cleaned_content), index=cleaned_content.index
    )

    ### 4. Model Training
    print(f"Training Model with {classification_model} Classifier")
    model_container = create_model_container(
        model=classification_model, bucket_names=bucket_names
    )
    trained_model = training_procedure(model_container, X_train, y_train, bucket_names)

    return trained_model, features_pipeline, bucket_names


def callback(message: Message):

    """
    callback

    Logic to handle incoming messages

    ACK:
        - short articles
        - articles that the model assigned no buckets to
        - articles which already have assigned buckets in Contentful
        - articles for which new buckets got successfully published
          in Contentful
    NACK:
        - if error occured when writing buckets to Contentful
        - if error occured when publishing an article with new buckets
          in Contentful

    """

    article_id = message.attributes["contentful_id"]
    print(f"\n[new] Received article (id: {article_id})")  # log msg

    print("...Retrieving Article's Body Field")
    article = Article(article_id)
    article_body = article.fetch_body()

    print("...Checking Article's Length")
    is_too_short = check_if_short(
        article_body, model_threshold=int(getenv("LENGTH_THRESHOLD", "250"))
    )
    if is_too_short:
        print("[ACK] Too short to run the model.")
        message.ack()

    print("...Assigning buckets")
    assigned_buckets = assign_buckets(
        article_id, article_body, TRAINED_MODEL, FEATURES_PIPELINE, BUCKET_NAMES
    )
    if assigned_buckets is None:
        print("[ACK] Model assigned no buckets")  # assuming some articles can have none
        message.ack()

    buckets_to_write = get_buckets_to_write(article, assigned_buckets)
    if not buckets_to_write:
        print("[ACK] The buckets already exist")
        message.ack()

    print("...Writing New Labels to Contentful")
    is_successfuly_processed = write_buckets(article, buckets_to_write)
    if not is_successfuly_processed:
        print(
            "[!NACK] Error occured when assigning/publishing buckets. Message will be re-delivered"
        )
        message.nack()
    else:
        print("[ACK]")
        message.ack()


if __name__ == "__main__":

    env = load_dotenv("./config/.env")
    print = partial(print, flush=True)  # always flush the stream when logging

    ### Train Model
    TRAINED_MODEL, FEATURES_PIPELINE, BUCKET_NAMES = train_model()

    ### Set Up Listener
    subscriber = SubscriberClient()
    subscription_path = (
        "projects/hazel-tea-194609/subscriptions/bucketeer-{environment}".format(
            environment=getenv("ENVIRONMENT_ID"),
        )
    )
    streaming_pull_future = subscriber.subscribe(subscription_path, callback=callback)

    print(f"\nListening for messages on {subscription_path}...")
    with subscriber:
        streaming_pull_future.result()  # listen indefinitely
