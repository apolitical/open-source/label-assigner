import sys
from os import getenv

from contentful_management import Client, Link

from typing import List, Optional


class Article:
    """
    A class used to represent an Article entry in Contentful.
    """

    def __init__(self, entry_id: str):
        """
        INPUT:
            entry_id - id of the article as assigned by Contentful.

        INSTANCE VARIABLES:
            entry - object representing the Contentful article.
            _buckets_initial_num - number of buckets when the article appears for pub/sub.
        """
        self.entry = self._fetch_articles().find(entry_id)
        self._buckets_initial_num = len(self._initialise_buckets())

    def _fetch_articles(self):
        """
        OUTPUT:
            entries - all recent entries in Contentful.
        """
        contentful_client = Client(getenv("MANAGEMENT_API_TOKEN", "fake_token"))
        entries = contentful_client.entries(
            getenv("SPACE_ID", "txbhe1wabmyx"), getenv("ENVIRONMENT_ID", "beta")
        )
        return entries

    def _initialise_buckets(self):
        """
        Creates a buckets field under the article if the field does not exist.

        OUTPUT:
            buckets_field - content of article's buckets field.
        """
        self._localized_field_below()
        buckets_field = getattr(self.entry, "buckets", list())
        self.entry.buckets = buckets_field
        return buckets_field

    def fetch_body(self) -> str:
        """
        OUTPUT:
            body_field - content of the article's body field. If the field does not exist, returns an empty string.
        """
        self._localized_field_below()
        body_field = getattr(self.entry, "body", str())
        return body_field

    def fetch_buckets(self) -> List[str]:
        """
        Retrieves buckets assigned to the article in a human-readable format.

        OUTPUT:
            bucket_names - list containing bucket names.
        """
        if self._buckets_initial_num > 0:
            self._localized_field_below()
            bucket_names = [
                self._get_bucket_name_by_id(bucket.id) for bucket in self.entry.buckets
            ]
        else:
            bucket_names = list()
        return bucket_names

    def add_bucket(self, bucket_name: str) -> bool:
        """
        Adds a new bucket object to the article's buckets field.

        INPUT:
            bucket_name - name of the bucket.

        OUTPUT:
            Returns True if the bucket successfully appended and False, if not.
        """
        # Create new entry representing Bucket in Contentful
        bucket_id = self._get_bucket_id_by_name(bucket_name)
        new_bucket = Link(
            {"sys": {"type": "Link", "linkType": "Entry", "id": bucket_id}}
        )
        # Add bucket
        try:
            self._localized_field_below()
            self.entry.buckets.append(new_bucket)
            self.entry.save()
            print(f"[done] Bucket '{bucket_name}' appended")
            return True
        except Exception as error:
            print(
                f"The following error occured when adding bucket '{bucket_name}': {error}"
            )
            return False

    def validate_buckets(self, bucket_list: List[str]) -> bool:
        """
        Checks if buckets were successfully appended to the article.

        INPUT:
            bucket_list - list of bucket names that had to be assigned.

        OUTPUT:
            is_num_same - True if buckets exist and False, if not.
        """
        buckets_to_assign_num = len(bucket_list)
        buckets_curr_num = len(self._initialise_buckets())
        is_num_same = (
            self._buckets_initial_num + buckets_to_assign_num == buckets_curr_num
        )
        return is_num_same

    def publish(self) -> bool:
        """
        Changes the article's status from Draft to Published, making changes visible to external services.

        OUTPUT:
            Returns True if successfully published and False, if not.
        """
        try:
            self.entry.publish()
            print(f"[done] All changes published")
            return True
        except:
            print(f"Error publishing buckets", file=sys.stderr)  # track this error
            return False

    def _localized_field_below(self):
        """
        Force the entry to locale before accessing >nested< localized fields.
        Way to avoid 'AttributeError: 'Entry' object has no attribute [...]'
        Ref: https://github.com/contentful/contentful-management.py/issues/51
             https://contentful.github.io/contentful-management.py/#entries
        """
        self.entry.sys["locale"] = "en"

    def _get_bucket_id_by_name(self, name: str) -> str:
        """
        Finds id of the bucket entry by its name.

        INPUT:
            name - bucket name.

        OUTPUT:
            bucket_id - id of the bucket entry in Contentful.
        """
        search_res = self._fetch_articles().all(
            {
                "content_type": "bucket",
                "fields.name": name,
                "select": "sys.id",
            }
        )
        bucket_id = search_res[0].id
        return bucket_id

    def _get_bucket_name_by_id(self, id: str) -> str:
        """
        Finds name of the bucket entry by its id.

        INPUT:
            id - bucket id.

        OUTPUT:
            bucket_name - name of the bucket entry in Contentful.
        """
        bucket_article = self._fetch_articles().find(id)
        bucket_article.sys["locale"] = "en"
        bucket_name = bucket_article.name
        return bucket_name
