# bucketeer.modelling

# sklearn imports
from sklearn.linear_model import SGDClassifier, LogisticRegression
from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV

# imblearn imports
from imblearn.over_sampling import SMOTE, ADASYN
from imblearn.pipeline import Pipeline as imbPipeline

# typing imports
from pandas import DataFrame, Series
from typing import Dict, Union

# modelling parameters imports
from bucketeer.modelling_params import (
    get_grid_params,
    get_sgd_kwargs,
    get_lr_kwargs,
    get_svc_kwargs,
)


def create_model_container(
    bucket_names: Series, model: str = "SGD+ADASYN"
) -> Dict[str, Union[SGDClassifier, GridSearchCV, LogisticRegression, SVC]]:

    """
    create_model_container

    Function to return a dictionary containing initialized model classifiers for
     each bucket in bucket_names

    INPUTS:
        bucket_names - pandas Series containing bucket names indexed by bucket id
        model - keyword indicating which classification model to use
                (either "BASELINE", "GRIDSEARCH", "SGD+SMOTE", "SGD+ADASYN",
                 "LR" or "SVM"])

    OUTPUTS:
        model_container - dict containing classifier for each bucket

    """

    model = model.upper()
    if model not in ["BASELINE", "GRIDSEARCH", "SGD+SMOTE", "SGD+ADASYN", "LR", "SVM"]:
        model = "SGD+SMOTE"  # default to SGD+SMOTE if invalid model type given

    model_container = dict()

    if model == "BASELINE":
        sgd_args = get_sgd_kwargs(baseline=True)

        for bucket in bucket_names:
            model_container[bucket] = SGDClassifier(**sgd_args)

    elif model == "GRIDSEARCH":
        sgd_args = get_sgd_kwargs()
        grid_params = get_grid_params()

        for bucket in bucket_names:
            model_container[bucket] = GridSearchCV(
                SGDClassifier(**sgd_args), grid_params, cv=5, scoring="f1"
            )

    elif model == "SGD+SMOTE":
        sgd_args = get_sgd_kwargs()
        grid_params = get_grid_params()

        for bucket in bucket_names:
            pipe = imbPipeline(
                [("smote", SMOTE(random_state=39)), ("sgd", SGDClassifier(**sgd_args))]
            )
            model_container[bucket] = GridSearchCV(
                pipe, grid_params, cv=5, scoring="f1"
            )

    elif model == "SGD+ADASYN":
        sgd_args = get_sgd_kwargs()
        grid_params = get_grid_params()

        for bucket in bucket_names:
            pipe = imbPipeline(
                [
                    ("adasyn", ADASYN(random_state=39)),
                    ("sgd", SGDClassifier(**sgd_args)),
                ]
            )
            model_container[bucket] = GridSearchCV(
                pipe, grid_params, cv=5, scoring="f1"
            )

    elif model == "LR":
        lr_args = get_lr_kwargs()

        for bucket in bucket_names:
            model_container[bucket] = LogisticRegression(**lr_args)

    elif model == "SVM":
        svc_args = get_svc_kwargs()

        for bucket in bucket_names:
            model_container[bucket] = SVC(**svc_args)

    return model_container


def training_procedure(
    model_container: Dict[
        str, Union[SGDClassifier, GridSearchCV, LogisticRegression, SVC]
    ],
    X_train: DataFrame,
    y_train: DataFrame,
    bucket_names: Series,
) -> Dict[str, Union[SGDClassifier, GridSearchCV, LogisticRegression, SVC]]:

    """
    training_procedure

    Function that trains each classifier model and returns a dictionary of the
    trained models for each bucket.

    INPUTS:
        model_container - dict containing initialized classifier models
        X_train - pandas DataFrame indexed by article slug, containing reduced fe-
                  ature set for prediction
        y_train - pandas DataFrame indexed by article slug, indicating to which (if
                  any) buckets training content belongs
        bucket_names - pandas Series containing bucket names indexed by bucket id

    OUTPUTS:
        trained_models - dict containing the trained classifiers for each bucket

    """

    trained_models = {
        bucket: model_container[bucket].fit(X_train, y_train[bucket])
        for bucket in bucket_names
    }

    return trained_models


def prediction_procedure(
    trained_models: Dict[
        str, Union[SGDClassifier, GridSearchCV, LogisticRegression, SVC]
    ],
    X_test: DataFrame,
    bucket_names: Series,
) -> DataFrame:

    """
    prediction_procedure

    Function that uses the trained classifiers for each bucket to predict which
    bucket(s) an article will fall into

    INPUTS:
        trained_models - dict containing the trained classifiers for each bucket
        X_test - pandas DataFrame indexed by post ID, containing the transformed
                 features of an article
        bucket_names - pandas Series containing bucket names indexed by bucket id

    OUTPUTS:
        predicted - pandas DataFrame indexed by post ID and columned by buckets,
                     each cell is populated with True/False, with True indicati-
                     ng if a post belongs to a bucket.

    """

    predicted = DataFrame(columns=bucket_names, index=X_test.index)

    for bucket in bucket_names:
        predicted[bucket] = trained_models[bucket].predict(X_test)

    return predicted


def labelling_procedure(predicted: DataFrame) -> DataFrame:

    """
    labelling_procedure

    Function to melt a DataFrame. 'Melting' is the inverse procedure to pivotin-
    g, i.e. melting will map multiple columns into one column, subject to some
    criterion. In this case, we create a 'label' column.

    Where a post ought to have a bucket assigned (according to the True/False l-
    abels in the input columns), a record is created associating that post's slug
    with the label (formerly column name) for that bucket.

    INPUTS:
        predicted - pandas DataFrame indexed by post ID and columned by buckets,
                     each cell is populated with True/False, with True indicati-
                     ng if a post belongs to a bucket.

    OUTPUTS:
        melted - pandas DataFrame indexed by post ID, the only column "label" i-
                 ndicating the bucket that post is predicted to belong to. Same
                 post can belong to more than one bucket, thus some indices
                 are duplicated.

    """

    melted = (
        predicted.reset_index()
        .melt(id_vars=["post_id"], var_name="label")
        .query("value == True")
        .set_index("post_id")[["label"]]
    )

    return melted
