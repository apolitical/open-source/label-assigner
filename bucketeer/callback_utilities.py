# bucketeer.callback_utilities
# helper functions used by callback() in bucketeer.app

from pandas import DataFrame, Series

from bucketeer.pre_processing import (
    get_clean_text,
)
from bucketeer.modelling import (
    prediction_procedure,
    labelling_procedure,
)
from bucketeer.contentful_api import Article

# type checking imports
from typing import Dict, List, Sequence, Optional
from pandas import DataFrame
from sklearn.pipeline import Pipeline
from sklearn.linear_model import SGDClassifier


def check_if_short(article_body: str, model_threshold: int) -> bool:

    """
    check_if_short

    INPUTS:
        article_body - the body of the article
        model_threshold - minimum number of words the article should
                        have to run the model

    OUTPUTS:
        returns True if the article is too short to run the model
        and False otherwise

    """

    words_num = len(article_body.split())

    if words_num < model_threshold:
        print(f"Number of words: {words_num}. Model threshold: {model_threshold}")
        return True
    else:
        return False


def assign_buckets(
    article_id: str,
    article_body: str,
    model: Dict[str, SGDClassifier],
    features_pipeline: Pipeline,
    bucket_names: Sequence[str],
) -> Optional[DataFrame]:

    """
    assign_buckets

    Assigns bucket labels to a Contentful article

    INPUTS:
        article_id - Contentful ID of the article
        article_body - the body of the article
        model - trained classification model
        features_pipeline - object with a fitted vectorizer
        bucket_names - bucket labels to assign

    OUTPUTS:
        assigned_buckets - buckets assigned to a Contentful article by the model,
                or None if none were assigned

    """

    article_body = Series(
        data=[article_body], index=[article_id]
    )  # Series required by further function
    article_body.index.rename("post_id", inplace=True)  # type: ignore

    clean_text = get_clean_text(article_body)
    features = DataFrame(
        features_pipeline.transform(clean_text), index=clean_text.index
    )
    predicted = prediction_procedure(model, features, bucket_names=bucket_names)
    assigned_buckets = labelling_procedure(predicted)

    has_buckets_assigned = len(assigned_buckets.index) != 0
    if not has_buckets_assigned:
        assigned_buckets = None

    return assigned_buckets


def get_buckets_to_write(article: Article, assigned_buckets: DataFrame) -> List[str]:

    """
    get_buckets_to_write

    INPUTS:
        article - instantiated Contentful article object
        assigned_buckets - buckets assigned to the article by the model

    OUTPUTS:
        buckets_to_write - buckets that should be written to Contentful

    """

    assigned_buckets = set(assigned_buckets["label"])
    current_buckets = set(article.fetch_buckets())
    buckets_to_write = list(assigned_buckets - current_buckets)

    return buckets_to_write


def write_buckets(article: Article, buckets_to_assign: List[str]) -> bool:

    """
    write_buckets

    Writes bucket labels to an article in Contentful.

    INPUTS:
        article - object representing a Contentful article.
        assigned_buckets - buckets assigned to a Contentful article by the model.

    OUTPUTS:
        Communicate status to pub/sub callback: True to ACK, False to NACK.

    """

    ### Writing New Labels to Contentful
    for bucket in buckets_to_assign:
        is_bucket_appended = article.add_bucket(bucket)
        if not is_bucket_appended:
            return False  # NACK - error

    is_every_bucket_published = article.publish()
    if not is_every_bucket_published:
        return False  # NACK - error

    ### Validate Assignments in Contentful
    has_new_buckets = article.validate_buckets(buckets_to_assign)

    return has_new_buckets
