# bucketeer.pre_processing

# basic imports
import numpy as np
import re

# HTML parsing imports
from bs4 import BeautifulSoup, Comment

# Natural Language ToolKit imports
from nltk import download
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk import stem, tag, pos_tag

# NLTK downloads (it won't re-download them if you already have them)
_ = download("stopwords")
_ = download("averaged_perceptron_tagger")
_ = download("wordnet")
_ = download("punkt")

# type-hinting
from pandas import Series, DataFrame
from typing import Sequence


def html_to_text(html: str) -> str:

    """
    html_to_text

    This function processes the raw text of an article and removes
    HTML comments, scripts, style tags, and objects. The cleaned text
    is returned

    INPUTS:
        html - character string containing raw HTML

    OUTPUTS:
        text - clean text output

    """

    soup = BeautifulSoup(html, "lxml")

    # Remove commented HTML
    elements = soup.findAll(text=lambda txt: isinstance(txt, Comment))
    [element.extract() for element in elements]

    # Remove script tags and contents
    elements = soup.findAll("script")
    [element.extract() for element in elements]

    elements = soup.findAll("noscript")
    [element.extract() for element in elements]

    # Remove style tags and contents
    elements = soup.findAll("style")
    [element.extract() for element in elements]

    # Remove objects and contents
    elements = soup.findAll("object")
    [element.extract() for element in elements]

    return soup.get_text()


def char_test(character: str) -> bool:

    """
    char_test

    Returns True if input is an alphanumeric character or a
    dash or an apostrophe, False otherwise

    """

    if character.isalnum():
        return True
    if character.isspace():
        return True
    if character in ["-", "'"]:
        return True

    return False


def remove_stopwords(content: str, stopwords: Sequence[str]) -> str:

    """
    remove_stopwords

    This function removes stopwords in a list from the input document

    INPUTS:
        content - character string: a document to be cleaned of stopwords
        stopwords - list of stopwords to remove

    OUTPUTS:
        stopped_content - content with all stopwords removed


    """

    stopped_content = " ".join(
        [word for word in word_tokenize(content) if word not in stopwords]
    )

    return re.sub(" '", "'", stopped_content)


def filter_training_data(articles: Series, length_threshold: int) -> Series:
    """
    filter_training_data

    Prepares data for training.

    INPUTS:
        articles - pandas Series indexed by article slug, containing raw
                   HTML-formatted content from the database
        length_threshold - minimum number of words an article should h-
                           ave so it's included as modelling data

    OUTPUTS:
        filtered_content - pandas Series indexed by article slug, conta-
                            ining processed texts as value for each post.

    """

    clean_content = articles.map(html_to_text)

    not_too_short = clean_content.str.split().map(len).ge(length_threshold)

    not_weekly_briefing = clean_content.str.contains("Top Stories\n").map(
        np.logical_not
    )

    filtered_content = clean_content.loc[not_too_short & not_weekly_briefing]

    return filtered_content


def get_clean_text(articles: Series) -> Series:

    """
    get_clean_text

    Clean content of our articles.

    INPUTS:
        articles - pandas Series indexed by article slug, containing article body.

    OUTPUTS:
        cleaned_content - pandas Series indexed by article slug, conta-
                            ining clean texts as value for a post.

    """

    english_stopwords = stopwords.words("english")

    # long method chain consisting of tweaks to content before featurisation
    cleaned_content = (
        articles.str.replace(
            r"(?s)^MindLab.*our feed on government innovation", "", regex=True
        )  # clean out the MindLab intros
        .str.replace(
            "•[^\n]+(?:more like this|write for us)[^\n]+[\n]", " \n ", regex=True
        )  # remove in-line prompts
        .str.replace(
            r"\bsaid\b", " ", regex=True
        )  # try stripping out the word 'said' as it seems to cause some problems
        .str.replace(
            r"\b[\w._%+-]+@[\w.-]+\.[\w]{2,}\b", "", regex=True
        )  # remove any email addresses
        .str.replace(
            "'", "'", regex=False
        )  # auto-escaping of apostrophes needs fixing up-front
        .str.lower()  # map to lowercase
        .str.replace("\n", " ", regex=False)  # replace new-line with space
        .str.replace("\r", " ", regex=False)  # replace carriage-return with space
        .str.replace(
            "\xa0", " ", regex=False
        )  # replace non-line-breaking-space with space
        .str.replace("—", " ", regex=False)  # replace m-dash with a space
        .str.replace(
            r"\(\s*[Pp]icture credit[^)]+\)", "", regex=True
        )  # remove picture credits
        .str.replace(r"\[[^]]+\]", "", regex=True)  # square bracket stripping
        .map(
            lambda text: "".join([c for c in text if char_test(c)])
        )  # strip out punctuation, except dashes and apostrophes
        .map(
            lambda text: remove_stopwords(text, english_stopwords)
        )  # remove the most common English words
        .str.strip()  # remove leading and trailing whitespace
    )

    return cleaned_content


def flatten_labels(y_raw: Series, bucket_names: Series) -> DataFrame:

    """
    flatten_labels

    Function to flatten the labels (i.e. buckets) into table with post
    slug as indices and bucket names as columns. Each cell has value True/False,
    indicating if a post belongs to a bucket (True) or not (False).

    INPUTS:
        y_raw - pandas Series indexed by article slug, each row value indicates
                the buckets a post belongs to
        bucket_names - pandas Series with bucket names indexed by bucket id

    OUTPUTS:
        y_flattened - pandas DataFrame indexed by post slug, each cell value
                indicates if a post (index) belongs to a bucket (column) or not

    """

    bucket_ids = bucket_names.index.tolist()
    y_flattened = DataFrame(columns=bucket_ids, index=y_raw.index)

    for bucket in bucket_ids:
        y_flattened[bucket] = y_raw.map(lambda buckets: bucket in buckets)

    y_flattened.columns = bucket_names

    return y_flattened
