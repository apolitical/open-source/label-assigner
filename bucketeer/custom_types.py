from typing_extensions import TypedDict
from typing import Tuple, List, Optional, Callable, Union

TfidfKwargsType = TypedDict(
    "TfidfKwargsType",
    {
        "input": str,
        "encoding": str,
        "decode_error": str,
        "strip_accents": str,
        "lowercase": bool,
        "preprocessor": Optional[Callable],
        "tokenizer": Optional[Callable],
        "analyzer": str,
        "stop_words": str,
        "token_pattern": str,
        "ngram_range": Tuple[int, int],
        "max_df": Union[int, float],
        "min_df": Union[int, float],
        "max_features": Optional[int],
        "vocabulary": Optional[Callable],
        "binary": bool,
        "norm": str,
        "use_idf": bool,
        "smooth_idf": bool,
        "sublinear_tf": bool,
    },
)


ParamGridType = TypedDict(
    "ParamGridType",
    {
        "sgd__loss": List[str],
        "sgd__alpha": List[float],
        "sgd__tol": List[float],
        "sgd__epsilon": List[float],
    },
)

SGDkwargsType = TypedDict(
    "SGDkwargsType",
    {
        "penalty": str,
        "l1_ratio": float,
        "fit_intercept": bool,
        "max_iter": int,
        "shuffle": bool,
        "verbose": int,
        "n_jobs": Optional[int],
        "random_state": int,
        "learning_rate": str,
        "eta0": float,
        "power_t": float,
        "early_stopping": bool,
        "validation_fraction": float,
        "n_iter_no_change": int,
        "class_weight": Optional[dict],
        "warm_start": bool,
        "average": bool,
        "loss": str,
        "alpha": float,
        "tol": float,
        "epsilon": float,
    },
    total=False,
)

LRkwargsType = TypedDict(
    "LRkwargsType",
    {
        "penalty": str,
        "dual": bool,
        "tol": float,
        "C": float,
        "fit_intercept": bool,
        "intercept_scaling": int,
        "class_weight": Optional[dict],
        "random_state": Optional[int],
        "solver": str,
        "max_iter": int,
        "multi_class": str,
        "verbose": int,
        "warm_start": bool,
        "n_jobs": Optional[int],
        "l1_ratio": Optional[float],
    },
)

SVCkwargsType = TypedDict(
    "SVCkwargsType",
    {
        "C": float,
        "kernel": str,
        "degree": int,
        "gamma": str,
        "coef0": float,
        "shrinking": bool,
        "probability": bool,
        "tol": float,
        "verbose": bool,
        "max_iter": int,
        "decision_function_shape": str,
        "random_state": Optional[int],
    },
)
