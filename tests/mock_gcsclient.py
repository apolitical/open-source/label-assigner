# mock_gcsclient
# provides mock classes for simulating connection to GCS


class MockBlob:
    def __init__(self, name):
        self.filename = name

    @property
    def name(self):
        return self.filename

    def download_as_bytes(self):
        if self.name == "3/data.json":  # last in the order
            return '{"mock-key": "mock-value"}'
        else:
            return '{"wrong-key": "wrong-value"}'


class MockBucket:
    def __init__(self, bucket_name):
        self.bucket_name = bucket_name

    def blob(self, filename):
        return MockBlob(filename)

    def list_blobs(self, prefix=None, delimiter=None):
        if self.bucket_name == "contentful_content":
            return [
                MockBlob(name="1/data.html"),
                MockBlob(name="2/data.json"),
                MockBlob(name="3/data.json"),
                MockBlob(name="3/data.html"),
            ]
        else:
            return []


class MockClient:
    def bucket(self, bucket_name):
        return MockBucket(bucket_name)
