# # test_gcs.py
# # tests for bucketeer/gcs.py

import pytest

from pandas import Series
from pandas.testing import assert_series_equal

from copy import deepcopy

from bucketeer.gcs import (
    merge_dicts,
    extend_content,
    get_latest_contentful_entries,
    get_relevant_articles,
)

from tests.mock_data import mock_contentful_data
from tests.mock_gcsclient import MockClient


@pytest.fixture
def mock_contentful():
    return deepcopy(mock_contentful_data)


@pytest.fixture
def mock_client():
    return MockClient()


def test_get_latest_contentful_entries(mock_client):
    result = get_latest_contentful_entries(mock_client)
    assert result == {"mock-key": "mock-value"}


def test_get_relevant_articles(mock_contentful):
    slug_to_body_map = {
        "article-slug-23": "a",
        "article-slug-129": "b",
        "recreation-revolution-why-wales": "Most everyone knows that play is an essential part.",
        "agribusiness-plus-parenting": "Integrating parenting classes with training in the business.",
    }

    slug_to_case_study_map = {
        "recreation-revolution-why-wales": "FAKE-CASE-STUDY-ID-ONE",
        "agribusiness-plus-parenting": "FAKE-CASE-STUDY-ID-TWO",
    }

    case_study_to_extra_body_map = {
        "FAKE-ID-3": "An eight year partnership between a non-profit.",
        "FAKE-ID-4": "Oslo has created an app that keeps children safe on the.",
    }

    assert get_relevant_articles(mock_contentful) == (
        slug_to_body_map,
        slug_to_case_study_map,
        case_study_to_extra_body_map,
    )


def test_merge_dicts():
    dict_left = {"slug-1": "id-1", "slug-2": "id-2", "slug-3": "id-3"}

    dict_right = {"id-1": "body-1", "id-2": "body-2", "id-4": "body-4"}

    final_map = {"slug-1": "body-1", "slug-2": "body-2"}
    expected = Series(final_map)

    result = merge_dicts(slug_id_map=dict_left, id_body_map=dict_right)

    assert_series_equal(expected, result)


def test_extend_content():
    articles = {"slug-1": "body-1", "slug-2": "body-2", "slug-3": "body-3"}

    extra = {"slug-1": "further-1", "slug-2": "further-2", "slug-4": "further-4"}
    extra_content = Series(extra)

    final_map = {
        "slug-1": "body-1 \n further-1",
        "slug-2": "body-2 \n further-2",
        "slug-3": "body-3 \n ",
    }
    expected = Series(final_map)
    expected.name = "full_content"
    expected.index.name = "article_slug"

    result = extend_content(articles=articles, extra_content=extra_content)

    assert_series_equal(expected, result)
