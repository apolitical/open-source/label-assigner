# test_contentful_api.py
# tests for bucketeer/contentful_api.py

import pytest

from bucketeer.contentful_api import Article
from contentful_management import Entry


#### Mocks ####


def mock_init_no_buckets(self):
    self.entry = Entry(
        {
            "sys": {"type": "Entry", "id": "5JKV6K5xzIqtOUcmoDj1P2"},
        }
    )
    self._buckets_initial_num = 0


def mock_init_has_one_bucket(self):
    self.entry = Entry(
        {
            "sys": {"type": "Entry", "id": "5JKV6K5xzIqtOUcmoDj1P2"},
            "fields": {
                "buckets": {
                    "en": [
                        {
                            "sys": {
                                "type": "Link",
                                "linkType": "Entry",
                                "id": "3mTpPDVnSOLUClxoUEjESK",
                            }
                        },
                    ]
                },
            },
        }
    )
    self._buckets_initial_num = 1


def mock_init_has_two_buckets(self):
    self.entry = Entry(
        {
            "sys": {"type": "Entry", "id": "5JKV6K5xzIqtOUcmoDj1P2"},
            "fields": {
                "buckets": {
                    "en": [
                        {
                            "sys": {
                                "type": "Link",
                                "linkType": "Entry",
                                "id": "3mTpPDVnSOLUClxoUEjESK",
                            }
                        },
                        {
                            "sys": {
                                "type": "Link",
                                "linkType": "Entry",
                                "id": "MZ8lSG1bRyboYIpJ95Fm5",
                            }
                        },
                    ]
                },
            },
        }
    )
    self._buckets_initial_num = 2


#### Tests ####


## For self.fetch_body()
## For self._localized_field_below()
def test_fetch_body_has_body(monkeypatch):
    def mock_init_has_body_field(self):
        self.entry = Entry(
            {
                "sys": {"type": "Entry", "id": "5JKV6K5xzIqtOUcmoDj1P2"},
                "fields": {
                    "body": {
                        "en": "Social cohesion must be at the heart of levelling up"
                    }
                },
            }
        )

    monkeypatch.setattr(Article, "__init__", mock_init_has_body_field)
    assert (
        Article().fetch_body() == "Social cohesion must be at the heart of levelling up"
    )


def test_fetch_body_no_body(monkeypatch):
    def mock_init_no_body_field(self):
        self.entry = Entry(
            {
                "sys": {"type": "Entry", "id": "5JKV6K5xzIqtOUcmoDj1P2"},
            }
        )

    monkeypatch.setattr(Article, "__init__", mock_init_no_body_field)
    assert Article().fetch_body() == str()


## For self.fetch_buckets()
def test_fetch_buckets_none(monkeypatch):
    monkeypatch.setattr(Article, "__init__", mock_init_no_buckets)
    assert Article()._buckets_initial_num == 0
    assert Article().fetch_buckets() == list()


def test_fetch_buckets_exist(monkeypatch):
    def mock_get_bucket_name_by_id(self, id):
        return "fake name"

    monkeypatch.setattr(Article, "__init__", mock_init_has_two_buckets)
    monkeypatch.setattr(Article, "_get_bucket_name_by_id", mock_get_bucket_name_by_id)
    assert Article()._buckets_initial_num == 2
    bucket_names = Article().fetch_buckets()
    assert len(bucket_names) == 2

    monkeypatch.setattr(Article, "__init__", mock_init_has_one_bucket)
    assert Article()._buckets_initial_num == 1
    bucket_names = Article().fetch_buckets()
    assert len(bucket_names) == 1


## For self.validate_buckets()
def test_validate_buckets(monkeypatch):
    def mock_initialise_three(self):
        self._localized_field_below()
        self.entry.buckets = ["one", "two", "three"]
        return self.entry.buckets

    monkeypatch.setattr(Article, "__init__", mock_init_has_two_buckets)
    monkeypatch.setattr(Article, "_initialise_buckets", mock_initialise_three)
    assert Article()._buckets_initial_num == 2
    assert Article().validate_buckets(["one"]) == True  # 1 + 2 == 3
    assert Article().validate_buckets(["one", "two"]) == False  # 2 + 2 != 3
    assert Article().validate_buckets(["one", "two", "three"]) == False  # 3 + 2 != 3

    def mock_initialise_two(self):
        self._localized_field_below()
        self.entry.buckets = ["one", "two"]
        return self.entry.buckets

    monkeypatch.setattr(Article, "__init__", mock_init_has_one_bucket)
    monkeypatch.setattr(Article, "_initialise_buckets", mock_initialise_two)
    assert Article()._buckets_initial_num == 1
    assert Article().validate_buckets(["one"]) == True  # 1 + 1 == 2
    assert Article().validate_buckets(["one", "two"]) == False  # 2 + 1 != 2
