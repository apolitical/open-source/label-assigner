# test_pre_processing.py
# tests for bucketeer/pre_processing.py

import pytest

from pandas.testing import assert_frame_equal
from pandas import Series, DataFrame

from bucketeer.pre_processing import (
    html_to_text,
    char_test,
    remove_stopwords,
    filter_training_data,
    get_clean_text,
    flatten_labels,
)


#### tests ####
def test_html_to_text_strips_tags():
    mock_html = """
    <emph>Good morning and welcome!</emph>
    This is your all-new unit-testing experience,
    """
    text = html_to_text(mock_html)
    assert (
        text
        == """Good morning and welcome!
    This is your all-new unit-testing experience,
    """
    )


@pytest.mark.parametrize(
    "input, result",
    [("a", True), ("1", True), (" ", True), ("-", True), ("'", True), ("&", False)],
)
def test_char_test(input, result):
    assert char_test(input) == result


def test_remove_stopwords():
    mock_stopwords = ["táimid", "ag", "tástáil"]
    mock_content = "We are testing - táimid ag tástáil"
    result = remove_stopwords(mock_content, mock_stopwords)
    assert result == "We are testing -"


def test_filter_training_data():
    top_story = """
        Top Stories\n 
        MindLab our feed on government innovation
        The Remaining String
        """
    dummy_input = Series({0: top_story})
    result = filter_training_data(dummy_input, 2)
    assert len(result) == 0

    not_top_story = """
        MindLab our feed on government innovation
        The Remaining String
        """
    dummy_input = Series({0: not_top_story})
    result = filter_training_data(dummy_input, 2)
    assert len(result) == 1

    five_words = "one two three four five"
    dummy_input = Series({0: five_words})
    result = filter_training_data(dummy_input, 5)
    assert len(result) == 1
    result = filter_training_data(dummy_input, 6)
    assert len(result) == 0


def test_get_clean_text():
    dummy_text = """MindLab our feed on government innovation
        The Remaining String
        """

    dummy_input = Series({0: dummy_text})

    result = get_clean_text(dummy_input)
    assert result.iloc[0] == "remaining string"


def test_flatten_labels():
    y_raw_mock = {"slug-1": [2], "slug-2": [1, 2, 3], "slug-3": [3, 1]}
    ser_1 = Series(y_raw_mock)

    bucket_names_mock = {1: "bucket one", 2: "bucket two", 3: "bucket three"}
    ser_2 = Series(bucket_names_mock)

    final_map_mock = {
        "bucket one": [False, True, True],
        "bucket two": [True, True, False],
        "bucket three": [False, True, True],
    }
    expected = DataFrame(final_map_mock, index=["slug-1", "slug-2", "slug-3"])

    result = flatten_labels(y_raw=ser_1, bucket_names=ser_2)

    assert_frame_equal(expected, result)
