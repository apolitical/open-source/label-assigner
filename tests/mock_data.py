# mock_data
# provides mock data for our tests


#### data for DBLink MockResponses ####

simple_mock_keys = ["a", "b"]
simple_mock_data = [[1, "x"], [2, "y"], [3, "z"], [4, "a"], [5, "b"]]

bucket_name_keys = ["bucket_id", "name"]
bucket_name_data = [
    [1, "bucket name one"],
    [2, "bucket name two"],
    [3, "bucket name three"],
]

article_bucket_keys = ["article_slug", "bucket_id"]
article_bucket_data = [["slug-1", 5], ["slug-1", 2], ["slug-2", 5]]


#### data for GCS processing tests ####
# At least 50% of mock data entries are real JSON schemas.
# All IDs were replaced and most strings were shortened for test purposes.

mock_contentful_data_entries = [
    # Solution Article - no case study 1/2
    {
        "fields": {
            "slug": {
                "en": "article-slug-23",
                "es": "article-slug-23-es",
                "fr": "article-slug-23-fr",
            },
            "identifier": {"en": 23},
            "title": {"en": "title-23", "es": "title-23-es", "fr": "title-23-fr"},
            "subtitle": {"en": None},
            "type": {"en": "analysis"},
            "publishedDate": {
                "en": "2020-03-30 09:22:30 UTC",
                "es": "2020-03-30 09:22:30 UTC",
                "fr": "2020-03-30 09:22:30 UTC",
            },
            "humanTranslation": {"en": False},
            "body": {"en": "a", "es": "a", "fr": "a"},
        },
        "metadata": {},
        "sys": {"contentType": {"sys": {"id": "solutionArticle"}}},
    },
    # Solution Article - no case study 2/2
    {
        "fields": {
            "slug": {
                "en": "article-slug-129",
                "es": "article-slug-129-es",
                "fr": "article-slug-129-fr",
            },
            "identifier": {"en": 129},
            "title": {"en": "title-129", "es": "title-129-es", "fr": "title-129-fr"},
            "subtitle": {
                "en": "subtitle-129",
                "es": "subtitle-129-es",
                "fr": "subtitle-129-fr",
            },
            "type": {"en": "analysis"},
            "publishedDate": {
                "en": "2021-03-11 01:20:40 UTC",
                "es": "2021-03-11 01:20:40 UTC",
                "fr": "2021-03-11 01:20:40 UTC",
            },
            "humanTranslation": {"en": False},
            "body": {"en": "b", "es": "b", "fr": "b"},
        },
        "metadata": {},
        "sys": {"contentType": {"sys": {"id": "solutionArticle"}}},
    },
    # Solution Article of case-study type 1/2
    {
        "metadata": {"tags": []},
        "sys": {
            "space": {
                "sys": {"type": "Link", "linkType": "Space", "id": "FAKE-SPACE-ID"}
            },
            "id": "FAKE-ID-1",
            "type": "Entry",
            "createdAt": "2021-04-28T16:18:52.606Z",
            "updatedAt": "2021-05-11T12:02:08.443Z",
            "environment": {
                "sys": {"id": "master", "type": "Link", "linkType": "Environment"}
            },
            "publishedVersion": 7,
            "publishedAt": "2021-05-11T12:02:08.443Z",
            "firstPublishedAt": "2021-04-28T16:18:52.969Z",
            "createdBy": {
                "sys": {
                    "type": "Link",
                    "linkType": "User",
                    "id": "FAKE-ID-1",
                }
            },
            "updatedBy": {
                "sys": {
                    "type": "Link",
                    "linkType": "User",
                    "id": "FAKE-ID-1",
                }
            },
            "publishedCounter": 4,
            "version": 8,
            "publishedBy": {
                "sys": {
                    "type": "Link",
                    "linkType": "User",
                    "id": "FAKE-ID-1",
                }
            },
            "contentType": {
                "sys": {
                    "type": "Link",
                    "linkType": "ContentType",
                    "id": "solutionArticle",
                }
            },
        },
        "fields": {
            "title": {
                "en": "Recreation revolution: Why Wales enshrined kids'",
                "es": "Revolución de la recreación: por qué Gales consagró",
                "fr": "Révolution des loisirs: pourquoi le pays de Galles",
            },
            "slug": {
                "en": "recreation-revolution-why-wales",
                "es": "revolucion-de-la-recreacion-por",
                "fr": "revolution-des-loisirs-pourquoi",
            },
            "subtitle": {
                "en": "Local authorities in the UK nation must",
                "es": "Las autoridades locales en la nación",
                "fr": "Les autorités locales du Royaume-Uni",
            },
            "type": {"en": "case-study"},
            "identifier": {"en": 29383},
            "authorship": {"en": None},
            "humanTranslation": {"en": False},
            "publishedDate": {
                "en": "2018-09-07T15:13:48Z",
                "es": "2018-09-07T15:13:48Z",
                "fr": "2018-09-07T15:13:48Z",
            },
            "modifiedDate": {
                "en": "2018-09-07T15:13:48Z",
                "es": "2018-09-07T15:13:48Z",
                "fr": "2018-09-07T15:13:48Z",
            },
            "body": {
                "en": "Most everyone knows that play is an essential part.",
                "fr": "Presque tout le monde sait que le jeu est.",
            },
            "buckets": {
                "en": [
                    {
                        "sys": {
                            "type": "Link",
                            "linkType": "Entry",
                            "id": "FAKE-BUCKET-ID-ONE",
                        }
                    },
                    {
                        "sys": {
                            "type": "Link",
                            "linkType": "Entry",
                            "id": "FAKE-BUCKET-ID-TWO",
                        }
                    },
                ]
            },
            "image": {
                "en": {
                    "sys": {
                        "type": "Link",
                        "linkType": "Asset",
                        "id": "FAKE-IMAGE-ID-ONE",
                    }
                }
            },
            "caseStudy": {
                "en": {
                    "sys": {
                        "type": "Link",
                        "linkType": "Entry",
                        "id": "FAKE-CASE-STUDY-ID-ONE",
                    }
                }
            },
        },
    },
    # Solution Article of case-study type 2/2
    {
        "metadata": {"tags": []},
        "sys": {
            "space": {
                "sys": {"type": "Link", "linkType": "Space", "id": "FAKE-SPACE-ID"}
            },
            "id": "FAKE-ID-2",
            "type": "Entry",
            "createdAt": "2021-04-28T16:42:24.411Z",
            "updatedAt": "2021-06-07T22:20:01.692Z",
            "environment": {
                "sys": {"id": "master", "type": "Link", "linkType": "Environment"}
            },
            "publishedVersion": 11,
            "publishedAt": "2021-06-07T22:20:01.692Z",
            "firstPublishedAt": "2021-04-28T16:42:24.873Z",
            "createdBy": {
                "sys": {
                    "type": "Link",
                    "linkType": "User",
                    "id": "FAKE-ID-2",
                }
            },
            "updatedBy": {
                "sys": {
                    "type": "Link",
                    "linkType": "User",
                    "id": "FAKE-ID-2",
                }
            },
            "publishedCounter": 6,
            "version": 12,
            "publishedBy": {
                "sys": {
                    "type": "Link",
                    "linkType": "User",
                    "id": "FAKE-ID-2",
                }
            },
            "contentType": {
                "sys": {
                    "type": "Link",
                    "linkType": "ContentType",
                    "id": "solutionArticle",
                }
            },
        },
        "fields": {
            "title": {
                "en": "Agribusiness plus parenting classes cuts child",
                "es": "Agronegocios más clases infantil en Tanzania",
                "fr": "L'agroalimentaire et les cours de parentalité",
            },
            "slug": {
                "en": "agribusiness-plus-parenting",
                "es": "agronegocios-mas-clases-para",
                "fr": "l-agro-industrie-et-des-cours",
            },
            "subtitle": {
                "en": "The project could bring hope to rural communities struggling",
                "es": "El proyecto podría traer esperanza a las comunidades",
                "fr": "Le projet pourrait apporter de l'espoir aux communautés rurales",
            },
            "type": {"en": "case-study"},
            "identifier": {"en": 322770},
            "authorship": {"en": None},
            "humanTranslation": {"en": False},
            "publishedDate": {
                "en": "2018-10-18T16:07:18Z",
                "es": "2018-10-18T16:07:18Z",
                "fr": "2018-10-18T16:07:18Z",
            },
            "modifiedDate": {
                "en": "2018-10-18T16:07:18Z",
                "es": "2018-10-18T16:07:18Z",
                "fr": "2018-10-18T16:07:18Z",
            },
            "body": {
                "en": "Integrating parenting classes with training in the business.",
                "es": "La integración de las clases",
                "fr": "L'intégration de cours de parentalité à une formation dans.",
            },
            "buckets": {
                "en": [
                    {
                        "sys": {
                            "type": "Link",
                            "linkType": "Entry",
                            "id": "FAKE-BUCKET-ID-THREE",
                        }
                    },
                    {
                        "sys": {
                            "type": "Link",
                            "linkType": "Entry",
                            "id": "FAKE-BUCKET-ID-TWO",
                        }
                    },
                    {
                        "sys": {
                            "type": "Link",
                            "linkType": "Entry",
                            "id": "FAKE-BUCKET-ID-SIX",
                        }
                    },
                ]
            },
            "image": {
                "en": {
                    "sys": {
                        "type": "Link",
                        "linkType": "Asset",
                        "id": "image-id",
                    }
                }
            },
            "contacts": {
                "en": [
                    {
                        "sys": {
                            "type": "Link",
                            "linkType": "Entry",
                            "id": "contact-id",
                        }
                    }
                ]
            },
            "caseStudy": {
                "en": {
                    "sys": {
                        "type": "Link",
                        "linkType": "Entry",
                        "id": "FAKE-CASE-STUDY-ID-TWO",
                    }
                }
            },
            "seoDescription": {
                "en": "Rural development depends on future generations.",
                "es": "El desarrollo rural depende de las generaciones futuras.",
                "fr": "Le développement rural dépend des générations futures.",
            },
        },
    },
    # Case Study 1/2
    {
        "metadata": {"tags": []},
        "sys": {
            "space": {
                "sys": {"type": "Link", "linkType": "Space", "id": "FAKE-SPACE-ID"}
            },
            "id": "FAKE-ID-3",
            "type": "Entry",
            "createdAt": "2021-04-28T11:53:17.132Z",
            "updatedAt": "2021-04-28T11:53:17.510Z",
            "environment": {
                "sys": {"id": "master", "type": "Link", "linkType": "Environment"}
            },
            "publishedVersion": 1,
            "publishedAt": "2021-04-28T11:53:17.510Z",
            "firstPublishedAt": "2021-04-28T11:53:17.510Z",
            "createdBy": {
                "sys": {
                    "type": "Link",
                    "linkType": "User",
                    "id": "FAKE-ID-3",
                }
            },
            "updatedBy": {
                "sys": {
                    "type": "Link",
                    "linkType": "User",
                    "id": "FAKE-ID-3",
                }
            },
            "publishedCounter": 1,
            "version": 2,
            "publishedBy": {
                "sys": {
                    "type": "Link",
                    "linkType": "User",
                    "id": "FAKE-ID-3",
                }
            },
            "contentType": {
                "sys": {
                    "type": "Link",
                    "linkType": "ContentType",
                    "id": "solutionArticleCaseStudy",
                }
            },
        },
        "fields": {
            "title": {
                "en": "East Africa mobilises 27,000 volunteers",
                "es": "África Oriental moviliza a 27,000 voluntarios",
                "fr": "L'Afrique de l'Est mobilise 27 000 volontaires",
            },
            "results": {
                "en": "In their last report, published in 2015, Twaweza examined",
                "es": "En su último informe, publicado en 2015, Twaweza examinó",
                "fr": "Dans son dernier rapport, publié en 2015, Twaweza a examiné",
            },
            "keyParties": {
                "en": "The governments of Tanzania, Uganda and Kenya, Twaweza,",
                "es": "Los gobiernos de Tanzania, Uganda y Kenia, Twaweza,",
                "fr": "Les gouvernements de Tanzanie, d'Ouganda et du Kenya,",
            },
            "how": {
                "en": "Every year the NGO Twaweza, which is funded by the Swedish",
                "es": "Cada año, la ONG Twaweza, financiada por los gobiernos",
                "fr": "Chaque année, l'ONG Twaweza, qui est financée",
            },
            "where": {
                "en": "Tanzania, Uganda and Kenya",
                "es": "Tanzania, Uganda y Kenia",
                "fr": "Tanzanie, Ouganda et Kenya",
            },
            "beneficiaries": {"en": "Children", "es": "Niños", "fr": "Les enfants"},
            "cost": {
                "en": "Not available",
                "es": "No disponible",
                "fr": "Indisponible",
            },
            "completionStage": {
                "en": "Running since 2009",
                "es": "Corriendo desde 2009",
                "fr": "Fonctionne depuis 2009",
            },
            "makingItHappen": {
                "en": "The process of getting approvals and permissions can be challenging",
                "es": "El proceso para obtener aprobaciones y permisos puede,",
                "fr": "Le processus d'obtention des approbations",
            },
            "fullStory": {
                "en": "An eight year partnership between a non-profit.",
                "es": "Una asociación de ocho años entre una organización sin fines de lucro",
                "fr": "Un partenariat de huit ans entre un organisme sans but lucratif",
            },
        },
    },
    # Case Study 2/2
    {
        "metadata": {"tags": []},
        "sys": {
            "space": {
                "sys": {"type": "Link", "linkType": "Space", "id": "FAKE-SPACE-ID"}
            },
            "id": "FAKE-ID-4",
            "type": "Entry",
            "createdAt": "2021-04-28T11:54:12.640Z",
            "updatedAt": "2021-04-28T11:54:13.023Z",
            "environment": {
                "sys": {"id": "master", "type": "Link", "linkType": "Environment"}
            },
            "publishedVersion": 1,
            "publishedAt": "2021-04-28T11:54:13.023Z",
            "firstPublishedAt": "2021-04-28T11:54:13.023Z",
            "createdBy": {
                "sys": {
                    "type": "Link",
                    "linkType": "User",
                    "id": "FAKE-ID-4",
                }
            },
            "updatedBy": {
                "sys": {
                    "type": "Link",
                    "linkType": "User",
                    "id": "FAKE-ID-4",
                }
            },
            "publishedCounter": 1,
            "version": 2,
            "publishedBy": {
                "sys": {
                    "type": "Link",
                    "linkType": "User",
                    "id": "FAKE-ID-4",
                }
            },
            "contentType": {
                "sys": {
                    "type": "Link",
                    "linkType": "ContentType",
                    "id": "solutionArticleCaseStudy",
                }
            },
        },
        "fields": {
            "title": {
                "en": "Oslo keeps kids safe by making them 'secret smartphone agents'",
                "es": "Oslo mantiene a los niños seguros al convertirlos en 'agentes'",
                "fr": "Oslo protège les enfants en faisant d'eux des «agents secrets»",
            },
            "results": {
                "en": "The children have filed almost 6,000 reports",
                "es": "Los niños han presentado casi 6,000 informes",
                "fr": "Les enfants ont déposé près de 6 000 rapports",
            },
            "keyParties": {
                "en": "Oslo’s Agency of Urban Environment, the Institute for Transport,",
                "es": "La Agencia de Medio Ambiente Urbano de Oslo, el Instituto.",
                "fr": "Agence d'Oslo de l'environnement urbain, Institute",
            },
            "how": {
                "en": "The app turns",
                "es": "La aplicación convierte.",
                "fr": "L'application transforme",
            },
            "where": {
                "en": "Oslo, Norway",
                "es": "Oslo, Noruega",
                "fr": "Oslo, Norvège",
            },
            "beneficiaries": {"en": "Children", "es": "Niños", "fr": "Les enfants"},
            "cost": {
                "en": "Oslo spent $369,300 on the app in 2016",
                "es": "Oslo gastó $ 369,300 en la aplicación en 2016",
                "fr": "Oslo a dépensé 369 300 $ pour l'application en 2016",
            },
            "completionStage": {
                "en": "Running since 2015",
                "es": "Corriendo desde 2015",
                "fr": "Fonctionne depuis 2015",
            },
            "makingItHappen": {
                "en": "The biggest challenge was ensuring children's data was secure.\r\n",
                "es": "El mayor desafío fue garantizar que los datos de los niños.\r\n",
                "fr": "Le plus grand défi était de garantir la sécurité.\r\n",
            },
            "replication": {
                "en": "The app has been replicated across Norway.",
                "es": "La aplicación se ha replicado en toda Noruega.",
                "fr": "L'application a été répliquée dans toute la Norvège.",
            },
            "fullStory": {
                "en": "Oslo has created an app that keeps children safe on the.",
                "es": "Oslo ha creado una aplicación que mantiene a los niños",
                "fr": "Oslo a créé une application qui assure la sécurité des enfants».",
            },
        },
    },
    # Other Entries
    {
        "fields": {},
        "metadata": {},
        "sys": {"contentType": {"sys": {"id": "otherContents"}}},
    },
    {
        "fields": {},
        "metadata": {},
        "sys": {"contentType": {"sys": {"id": "otherThings"}}},
    },
    {
        "fields": {},
        "metadata": {},
        "sys": {"contentType": {"sys": {"id": "otherStuff"}}},
    },
]

mock_contentful_data = {
    "assets": ["", "", ""],
    "contentTypes": ["", "", ""],
    "editorInterfaces": ["", "", ""],
    "entries": mock_contentful_data_entries,
    "locales": [{"code": "en"}, {"code": "es"}, {"code": "fr"}],
    "tags": ["", "", ""],
}
