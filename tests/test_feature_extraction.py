# test_feature_extraction.py
# tests for bucketeer/feature_extraction.py

import pytest

from sklearn.pipeline import Pipeline

from bucketeer.feature_extraction import get_tfidf_kwargs, feature_engineering


def test_get_tfidf_kwargs_returns_a_dictionary():
    tfidf_kwargs = get_tfidf_kwargs(5, 0.4)
    assert isinstance(tfidf_kwargs, dict)


def test_feature_engineering():
    result = feature_engineering(
        tfidf_min_df=0, tfidf_max_df=0.99, nmf_latent_topics=30
    )
    assert isinstance(result, Pipeline)
    assert list(result.named_steps.keys()) == ["tfidf", "nmf", "norm", "skip_estimator"]
