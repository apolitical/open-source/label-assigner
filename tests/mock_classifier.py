# mock_classifier
# provides mock classes for simulating a sgd classifier in tests
import numpy as np


class MockSGD:
    @staticmethod
    def fit(x, y):
        return "dummy trained classifier"

    @staticmethod
    def predict(x):
        return np.asarray([True])
