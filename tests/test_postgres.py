# test_postgres.py
# tests for bucketeer/postgres.py

import pytest

from pandas import DataFrame, Series

from sqlalchemy.engine.base import Engine

from bucketeer.postgres import (
    get_training_data,
    get_bucket_names,
    DBLink,
    env2file,
    get_db_client_pg,
)
from tests.mock_dblink import (
    mock_init,
    mock_create_engine,
    mock_bad_init,
    MockConnection,
    MockDBLink,
    MockResult,
)


#### fixtures ####
@pytest.fixture
def dblink_dummy_instance(monkeypatch):
    monkeypatch.setattr(DBLink, "__init__", mock_init)
    dummy_connection_string = "postgresql+psycopg2://user:password@101.101.11.1/db"
    return DBLink(dummy_connection_string)


@pytest.fixture
def dblink_bad_instance(monkeypatch):
    monkeypatch.setattr(DBLink, "__init__", mock_bad_init)
    dummy_connection_string = "postgresql+psycopg2://user:password@101.101.11.1/db"
    return DBLink(dummy_connection_string)


@pytest.fixture
def dblink_engine_mocked(monkeypatch):
    monkeypatch.setattr(Engine, "__init__", mock_create_engine)
    dummy_connection_string = "postgresql+psycopg2://user:password@101.101.11.1/db"
    return DBLink(dummy_connection_string)


#### tests ####
def test_get_training_data(monkeypatch, dblink_dummy_instance):
    result = get_training_data(dblink_dummy_instance)
    assert isinstance(result, Series)
    assert result.iloc[0] == [5, 2]  # in tests/mock_data.py


def test_get_bucket_names(monkeypatch, dblink_dummy_instance):
    result = get_bucket_names(dblink_dummy_instance)
    assert isinstance(result, Series)
    assert result.iloc[0] == "bucket name one"  # in tests/mock_data.py


## DBLink
class TestDBLink:
    def test_DBLink_init_creates_engine(self, dblink_dummy_instance):
        assert hasattr(dblink_dummy_instance, "engine")

    def test_DBLink_original_init(self, dblink_engine_mocked):
        assert dblink_engine_mocked.engine.url == "dummy url"

    def test_DBLink_get_connection_raises_on_timeout(self, dblink_bad_instance):
        with pytest.raises(ConnectionError):
            conn = dblink_bad_instance.get_connection(timeout=0.1)

    def test_DBLink_returns_connection_if_status_good(self, dblink_dummy_instance):
        conn = dblink_dummy_instance.get_connection()
        assert isinstance(conn, MockConnection)

    def test_DBLink_execute_return(self, dblink_dummy_instance):
        test_query = "SELECT a, b FROM some_table WHERE c = 5;"
        result = dblink_dummy_instance.execute(test_query)
        assert isinstance(result, MockResult)

    def test_DBLink_execute_closes_connection(self, mocker, dblink_dummy_instance):
        test_query = "SELECT a, b FROM some_table WHERE c = 5;"
        mocker.spy(MockConnection, "close")
        result = dblink_dummy_instance.execute(test_query)
        assert MockConnection.close.called == True

    def test_DBLink_query_to_dataframe_calls_execute_method(
        self, mocker, dblink_dummy_instance
    ):
        test_query = "SELECT a, b FROM some_table WHERE c = 5;"
        mocker.spy(dblink_dummy_instance, "execute")
        table = dblink_dummy_instance.query_to_dataframe(test_query)
        dblink_dummy_instance.execute.assert_called_with(test_query)

    def test_DBLink_query_to_dataframe_returns_mock_data(self, dblink_dummy_instance):
        test_query = "SELECT a, b FROM some_table WHERE c = 5;"
        table = dblink_dummy_instance.query_to_dataframe(test_query)
        assert isinstance(table, DataFrame)
        assert list(table.columns) == ["a", "b"]

    def test_DBLink_query_to_dataframe_resource_error(self, dblink_dummy_instance):
        test_query = "ThrowResourceError"
        table = dblink_dummy_instance.query_to_dataframe(test_query)
        assert isinstance(table, DataFrame)
        assert table.empty


## env2file
def test_env2file_raises_when_variable_unset(monkeypatch):
    monkeypatch.delenv("MISSING_ENV_VARIABLE", raising=False)
    with pytest.raises(EnvironmentError):
        filepath = env2file("MISSING_ENV_VARIABLE")


def test_env2file_decodes_base64(monkeypatch):
    monkeypatch.setenv("ENCODED_VARIABLE", "SGVsbG8gV29ybGQ=")
    output_file = env2file("ENCODED_VARIABLE")
    with open(output_file, "r") as output_file_object:
        contents = output_file_object.read()
    assert contents == "Hello World\n"


# get_db_client_pg
class TestGetDBClientPG:
    def test_get_db_client_pg_raises_when_variable_unset(self, monkeypatch):
        monkeypatch.delenv("PG_CONNECTION_STRING", raising=False)
        with pytest.raises(EnvironmentError):
            get_db_client_pg()

    def test_get_db_client_pg_local(self, monkeypatch):
        monkeypatch.setenv("PG_CONNECTION_STRING", "dummy-cs")
        monkeypatch.setenv("RUNNING_LOCALLY", "True")
        monkeypatch.setenv("PG_SSL_CERT", "Z28KcmFpYmgKbWFpdGgKYWdhdA==")
        monkeypatch.setenv("PG_SSL_KEY", "aXMKZ28KZm9pcmZl")
        monkeypatch.setattr(DBLink, "__init__", MockDBLink.__init__)

        client = get_db_client_pg()

        cert_file = client.ssl.get("sslcert")
        key_file = client.ssl.get("sslkey")
        with open(cert_file, "r") as file:
            assert file.read() == "go\nraibh\nmaith\nagat\n"
        with open(key_file, "r") as file:
            assert file.read() == "is\ngo\nfoirfe\n"

        assert client.connection_string == "dummy-cs"
        assert client.withssl == True

    def test_get_db_client_pg_notlocal(self, monkeypatch):
        monkeypatch.setenv("RUNNING_LOCALLY", "False")
        monkeypatch.setenv("PG_CONNECTION_STRING", "dummy-cs")
        monkeypatch.setenv("PG_SSL_CERT", "Z28KcmFpYmgKbWFpdGgKYWdhdA==")
        monkeypatch.setenv("PG_SSL_KEY", "aXMKZ28KZm9pcmZl")
        monkeypatch.setattr(DBLink, "__init__", MockDBLink.__init__)

        client = get_db_client_pg()

        assert client.connection_string == "dummy-cs"
        assert client.withssl == True
