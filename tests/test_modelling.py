# test_modelling.py
# tests for bucketeer/modelling.py

import pytest

from pandas import DataFrame
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.svm import SVC
from tests.mock_classifier import MockSGD

from bucketeer.modelling import (
    create_model_container,
    training_procedure,
    prediction_procedure,
    labelling_procedure,
)

#### tests ####
@pytest.mark.parametrize("model", [("bad_model"), ("SGD"), ("LR"), ("SVM")])
def test_create_model_container(mocker, model):
    bucket_names = ["dummy-bucket 1", "dummy bucket 2"]
    mocker.spy(SGDClassifier, "__init__")
    mocker.spy(LogisticRegression, "__init__")
    mocker.spy(SVC, "__init__")

    result = create_model_container(bucket_names, model=model)
    if model == "LR":
        assert LogisticRegression.__init__.call_count == 2
    elif model == "SVM":
        assert SVC.__init__.call_count == 2
    else:
        assert SGDClassifier.__init__.call_count == 2
    assert isinstance(result, dict)


def test_training_procedure():
    bucket_names = ["dummy-bucket-1"]
    dummy_container = {"dummy-bucket-1": MockSGD()}
    dummy_X = DataFrame(data={"feature-1": [777, 888, 999]}, index=[123, 124, 125])
    dummy_y = DataFrame(
        data={
            "dummy-bucket-1": [True, True, False],
            "dummy-bucket-2": [False, True, False],
        },
        index=[123, 124, 125],
    )

    result = training_procedure(dummy_container, dummy_X, dummy_y, bucket_names)

    assert isinstance(result, dict)
    assert result["dummy-bucket-1"] == "dummy trained classifier"


def test_prediction_procedure():
    bucket_names = ["dummy-bucket-1"]
    dummy_trained = {"dummy-bucket-1": MockSGD()}
    dummy_X = DataFrame(data={"feature-1": [777], "feature-2": [888]}, index=[123])

    result = prediction_procedure(dummy_trained, dummy_X, bucket_names)

    assert isinstance(result, DataFrame)
    assert result.iloc[0, 0] == True


def test_labelling_procedure():
    bucket_names = ["dummy-bucket-1", "dummy-bucket-2"]
    dummy_pred = DataFrame(
        {"dummy-bucket-1": True, "dummy-bucket-2": False, "dummy-bucket-3": True},
        index=[123],
    )
    dummy_pred.index.name = "post_id"
    result = labelling_procedure(dummy_pred)

    assert isinstance(result, DataFrame)
    assert result.iloc[0, 0] == "dummy-bucket-1"
