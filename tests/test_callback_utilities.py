# test_callback_utilities.py
# tests for bucketeer/callback_utilities.py

from pandas.core.frame import DataFrame

from bucketeer.app import (
    check_if_short,
    get_buckets_to_write,
)
from bucketeer.contentful_api import Article


def test_check_if_short():
    article_body = "one two three four five"

    model_threshold = 5
    result = check_if_short(article_body=article_body, model_threshold=model_threshold)
    assert result == False

    model_threshold = 6
    result = check_if_short(article_body=article_body, model_threshold=model_threshold)
    assert result == True


def test_get_buckets_to_write(monkeypatch):
    monkeypatch.delattr(Article, "__init__")
    monkeypatch.setattr(
        Article, "fetch_buckets", lambda x: ["bucket one", "bucket two"]
    )
    article = Article()

    final_map_mock = {
        "label": ["bucket two", "bucket three"],
    }
    assigned_buckets = DataFrame(
        final_map_mock, index=["contentful_id", "contentful_id"]
    )

    result = get_buckets_to_write(article, assigned_buckets)
    expected = ["bucket three"]

    assert result == expected
