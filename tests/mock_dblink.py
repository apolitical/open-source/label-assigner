# mock_dblink
# provides mock classes for simulating a database connector in tests

from sqlparse import parse as sqlparse
from sqlparse.sql import Identifier
from pandas import DataFrame
from sqlalchemy.exc import ResourceClosedError
from sqlalchemy.sql.elements import TextClause

from tests.mock_data import (
    simple_mock_keys,
    simple_mock_data,
    bucket_name_keys,
    bucket_name_data,
    article_bucket_keys,
    article_bucket_data,
)


def get_selected_columns(query):

    """
    get_selected_columns

    Use sqlparse to extract selected columns from a SQL statement

    INPUTS:
        query - SQL query, formatted as a character string

    OUTPUTS:
        select_columns - list of column names selected

    """

    p = sqlparse(query.strip())[0]

    sublists = list(p.get_sublists())

    # should be the first sublist
    if isinstance(sublists[0], Identifier):
        return sublists[0].get_real_name()

    column_identifiers = list(sublists[0].get_identifiers())

    selected_columns = [c.get_real_name() for c in column_identifiers]

    return selected_columns


class MissingMockData(Exception):
    pass


class MockResult:
    """
    MockResult :: placeholder for a sqlalchemy ResultProxy instance
    """

    def __init__(self, sql):
        """
        __init__ method :: passes sql input to sql attribute
        """
        self.sql = sql
        self.rowcount = 1
        if self.sql == "ThrowResourceError":
            self.selected_columns = "ThrowResourceError"
        elif "INSERT INTO" in self.sql:
            self.selected_columns = None
        elif "DELETE FROM" in self.sql:
            self.selected_columns = None
        else:
            self.selected_columns = get_selected_columns(sql)

    def keys(self):
        """
        mock Result.keys method
        """
        if self.selected_columns == simple_mock_keys:
            return simple_mock_keys
        if self.selected_columns == bucket_name_keys:
            return bucket_name_keys
        if self.selected_columns == article_bucket_keys:
            return article_bucket_keys

        raise MissingMockData(
            f"Columns: {self.selected_columns} didn't match a test case"
        )

    def fetchall(self):
        """
        mock Result.fetchall method
        """
        if self.selected_columns == "ThrowResourceError":
            raise ResourceClosedError
        if self.selected_columns == simple_mock_keys:
            return simple_mock_data
        if self.selected_columns == bucket_name_keys:
            return bucket_name_data
        if self.selected_columns == article_bucket_keys:
            return article_bucket_data

        raise MissingMockData(
            f"Columns: {self.selected_columns} didn't match a test case"
        )

    def fetchone(self):
        """
        mock Result.fetchone method
        """
        if self.selected_columns == simple_mock_keys:
            return simple_mock_data[0]
        if self.selected_columns == bucket_name_keys:
            return bucket_name_data[0]
        if self.selected_columns == article_bucket_keys:
            return article_bucket_data[0]

        raise MissingMockData(
            f"Column: {self.selected_columns} didn't match a test case"
        )


class MockConnection:
    """
    MockConnection ::
        .execute method returns MockResult, has an empty .close method
    """

    def execute(self, sql):
        """
        mock Connection.execute method
        """
        if isinstance(sql, TextClause):
            return MockResult(sql.text)
        return MockResult(sql)

    def close(self):
        """
        mock Connection.close method
        """
        pass


class MockEngine:
    """
    MockEngine :: .connect method returns MockConnection
    """

    def connect(self):
        """
        mock Engine.connect method
        """
        return MockConnection()


class MockBadEngine(MockEngine):
    """
    MockBadEngine :: throws an error when .connect is called
    """

    def connect(self):  # overwrite .connect method
        raise ConnectionError("Failed")


def mock_result_init(self, sql):
    """
    mock MockResult.__init__ :: sets rowcount to 0 instead of 1
    """
    self.sql = sql
    self.rowcount = 0
    if self.sql == "ThrowResourceError":
        self.selected_columns = "ThrowResourceError"
    elif "INSERT INTO" in self.sql:
        self.selected_columns = None
    elif "DELETE FROM" in self.sql:
        self.selected_columns = None
    else:
        self.selected_columns = get_selected_columns(sql)


def mock_init(self, connection_string):
    """
    mock DBLink.__init__ :: replicates good behaviour
    """
    self.connection_string = connection_string
    self.engine = MockEngine()


def mock_bad_init(self, connection_string):
    """
    mock DBLink.__init__ :: replicates failed connection
    """
    self.connection_string = connection_string
    self.engine = MockBadEngine()


def mock_create_engine(self, pool, dialect, u, dummy_args=None):
    self.url = "dummy url"
    self.dialect = DataFrame({"name": ["dummy dialect"]})


class MockDBLink:
    """
    mock DBLink :: stores initialisation arguments for test purposes
    """

    def __init__(self, connection_string, withssl=True, ssl=None):
        self.connection_string = connection_string
        self.withssl = withssl
        self.ssl = ssl
